//
//  SchoolListViewModelTests.swift
//  NYCSchoolsTests
//
//  Created by Satao, Krunal on 16/03/23.
//

import XCTest

final class SchoolListViewModelTests: XCTestCase {
    
    var viewModel: SchoolListViewModel!
    var serviceManagerMock: ServiceManagerMock!
    
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        try super.setUpWithError()
        
        serviceManagerMock = ServiceManagerMock()
        serviceManagerMock.mockFileName = Constants.FileName.schoolList
        viewModel = SchoolListViewModel(serviceManager: serviceManagerMock)
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        try super.tearDownWithError()
        
        self.serviceManagerMock = nil
        self.viewModel = nil
        
    }
    
    func testFetchSchoolListSuccess() throws {
        // Testcase -
        let expectation = XCTestExpectation(description: "Fetch schools successfully")
        
        // Test fetchSchoolList function from mock file
        viewModel.fetchSchoolList()
        
        // Check Results and Add Assertions
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // Exactly one article should be loaded
            XCTAssertEqual(self.viewModel.schools.count, 1)
            
            // There should not be any errors
            XCTAssertNil(self.viewModel.error)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 2.0)
    }
    
    func testFetchSchoolListFailure() {
        // Testcase -
        let expectation = XCTestExpectation(description: "Fetch articles failure")
        serviceManagerMock.shouldReturnError = true
        
        // Test fetchSchoolList function from mock file
        viewModel.fetchSchoolList()
        
        // Check Results and Add Assertions
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            // No articles should be fetched if request fails
            XCTAssertEqual(self.viewModel.schools.count, 0)
            
            // ViewModel should be have error
            XCTAssertNotNil(self.viewModel.error)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 2.0)
    }
}



/// Dummy ServiceManager implementation to mock API response
class ServiceManagerMock: ServiceManagerProtocol {
    var shouldReturnError = false
    var mockFileName = ""
    
    func send<T>(_ request: T, completion: @escaping (Result<T.Response, Error>) -> Void) where T : APIRequest {
        if shouldReturnError {
            completion(.failure(AppError.unexpectedResponse))
            return
        }
        
        if let data = MockData.shared.loadJSONData(fileName: mockFileName) {
            do {
                let decodedData = try JSONDecoder().decode(T.Response.self, from: data)
                completion(.success(decodedData))
            } catch {
                completion(.failure(AppError.decodingError))
            }
        } else {
            completion(.failure(AppError.unexpectedResponse))
        }
    }
}

/// class to load mock data from json files
class MockData {
    static let shared = MockData()
    
    private init() {}
    
    func loadJSONData(fileName: String) -> Data? {
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return data
            } catch {
                DDLogger.sharedLogger.send("Error reading mock data from file: \(fileName)", logLevel: .error)
            }
        } else {
            DDLogger.sharedLogger.send("File not found: \(fileName)", logLevel: .error)
        }
        return nil
    }
}
