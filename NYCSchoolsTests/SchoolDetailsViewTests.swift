//
//  SchoolDetailsViewTests.swift
//  NYCSchoolsTests
//
//  Created by Satao, Krunal on 16/03/23.
//

import XCTest

final class SchoolDetailsViewTests: XCTestCase {
    
    var viewModel: SchoolDetailsViewModel!
    var serviceManagerMock: ServiceManagerMock!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        try super.setUpWithError()
        
        serviceManagerMock = ServiceManagerMock()
        serviceManagerMock.mockFileName = Constants.FileName.satScores
        
        // Load sample school first
        if let data = MockData.shared.loadJSONData(fileName: Constants.FileName.schoolList) {
            do {
                let mockSchools = try JSONDecoder().decode(SchoolListRequest.Response.self, from: data)
                if let selectedSchool =  mockSchools.first {
                    viewModel = SchoolDetailsViewModel(serviceManager: serviceManagerMock, school: selectedSchool)
                } else {
                    throw AppError.decodingError
                }
            } catch {
                throw AppError.emptyData
            }
        } else {
            throw AppError.unexpectedResponse
        }
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        try super.tearDownWithError()
        
        self.serviceManagerMock = nil
        self.viewModel = nil
        
    }
    
    func testFetchSATScorestSuccess() throws {
        // Testcase -
        let expectation = XCTestExpectation(description: "Fetch schools successfully")
        
        // Test SATScores function from mock file
        viewModel.fetchSATScoreDetails()
        
        // Check Results and Add Assertions
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // SAT scores should be fetched
            XCTAssertNotNil(self.viewModel.satScoreDetails)
            
            // There should not be any errors
            XCTAssertNil(self.viewModel.error)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 2.0)
    }
    
    func testFetchSchoolListFailure() {
        // Testcase -
        let expectation = XCTestExpectation(description: "Fetch articles failure")
        serviceManagerMock.shouldReturnError = true
        
        // Test fetchSchoolList function from mock file
        viewModel.fetchSATScoreDetails()
        
        // Check Results and Add Assertions
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            // SAT Scores should be nil
            XCTAssertNil(self.viewModel.satScoreDetails)
            
            // ViewModel should be have error
            XCTAssertNotNil(self.viewModel.error)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 2.0)
    }
}
