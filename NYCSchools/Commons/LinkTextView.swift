//
//  LinkTextView.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 16/03/23.
//

import SwiftUI

struct LinkTextView: View {
    var text: String

    var body: some View {
        Text(text)
            .padding(.vertical, 1.0)
            .font(.titleMedium)
            .foregroundColor(.blue)
    }
}

struct LinkTextView_Previews: PreviewProvider {
    static var previews: some View {
        SecondaryTextView(text: "Testing text")
    }
}
