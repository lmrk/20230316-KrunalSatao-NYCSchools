//
//  RounderCorners.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 16/03/23.
//

import SwiftUI

struct SectionHeaderTextView: View {

    // MARK: Variables -
    let text: String

    // MARK: View Body -
    var body: some View {
        Text(text)
            .bold()
            .foregroundColor(Color.darkTextPrimary)
            .font(.titleLarge)
            .padding(.bottom, 5.0)
    }
}

struct SectionHeaderTextView_Previews: PreviewProvider {
    static var previews: some View {
        SectionHeaderTextView(text: "Hello")
    }
}
