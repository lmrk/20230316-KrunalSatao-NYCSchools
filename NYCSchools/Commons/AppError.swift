//
//  EnvironmentManager.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 15/03/23.
//

import Foundation

/// API Errors
enum AppError: Swift.Error {
    case invalidURL
    case httpCode(Int)
    case unexpectedResponse
    case emptyData
    case noInternet
    case decodingError
}

// Extension for API Error
extension AppError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .invalidURL: return "Invalid URL"
        case let .httpCode(code): return "Unexpected HTTP code: \(code)"
        case .unexpectedResponse: return "Unexpected response from the server"
        case .emptyData: return "Data not available"
        case .noInternet: return "Please check internet connection"
        case .decodingError: return "Cannot parse response"
        }
    }
}
