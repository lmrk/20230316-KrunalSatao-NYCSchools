//
//  RoundedCardComponent.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 16/03/23.
//

import Foundation
import SwiftUI

struct RoundedCardComponent<Content: View>: View {
    let content: Content

    init(@ViewBuilder content: () -> Content) {
        self.content = content()
    }

    var body: some View {
        content
            .modifier(RoundedCorners())
    }
}
