//
//  RounderCorners.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 16/03/23.
//

import SwiftUI

struct PrimaryTextView: View {
    var text: String

    var body: some View {
        Text(text)
            .padding(.vertical, 1.0)
            .font(.titleLarge)
            .foregroundColor(Color.neutralBlack)
    }
}

struct PrimaryTextView_Previews: PreviewProvider {
    static var previews: some View {
        PrimaryTextView(text: "Primary Text")
    }
}
