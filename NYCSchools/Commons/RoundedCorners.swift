//
//  RounderCorners.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 16/03/23.
//

import Foundation
import SwiftUI

struct RoundedCorners: ViewModifier {
    func body(content: Content) -> some View {
        content
            .frame(maxWidth: .infinity,
                   alignment: .leading)
            .padding(.horizontal, 12.0)
            .padding(.vertical, 12.0)
            .background(.clear)
            .cornerRadius(5)
            .overlay(
                RoundedRectangle(cornerRadius: 5)
                    .stroke(.gray, lineWidth: 1)
            )
    }
}

