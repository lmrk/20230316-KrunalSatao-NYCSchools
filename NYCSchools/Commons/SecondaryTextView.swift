//
//  RounderCorners.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 16/03/23.
//

import SwiftUI

struct SecondaryTextView: View {
    var text: String

    var body: some View {
        Text(text)
            .padding(.vertical, 1.0)
            .font(.titleMedium)
            .foregroundColor(Color.neutralClay)
    }
}

struct SecondaryTextView_Previews: PreviewProvider {
    static var previews: some View {
        SecondaryTextView(text: "Testing text")
    }
}
