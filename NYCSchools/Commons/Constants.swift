//
//  Constants.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 16/03/23.
//

import Foundation

struct Constants {
    struct Spacing {
        static let leading = CGFloat(8)
        static let padding = CGFloat(10)
        static let imageSize = CGFloat(13)
    }
    
    struct FileName {
        static let schoolList = "SchoolList"
        static let satScores = "SATScores"
    }
    
    struct ExternalURL {
        static let web = "SchoolList"
        static let phone = "SATScores"
    }
    
    struct APIPath {
        static let schoolList = "/resource/s3k6-pzi2.json?$limit=20"
        static let satScores = "/resource/f9bf-2cp4.json?dbn="
    }
    
    struct Keys {
        static let apiKey = "51BlyaqFG39VV9VMBJl0jt90h"
    }
}
