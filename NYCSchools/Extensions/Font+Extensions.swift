
import SwiftUI

enum AppFonts: String {
    case openSansLight = "OpenSans-Light"
    case openSansRegular = "OpenSans-Regular"
    case openSansMedium = "OpenSans-Medium"
    case openSansSemiBold = "OpenSans-SemiBold"
    case openSansBold = "OpenSans-Bold"
    case openSansExtraBold = "OpenSans-ExtraBold"
}

extension Font {

    /// Title Fonts
    static var titleLarge: Font {
        return Font.custom(AppFonts.openSansExtraBold.rawValue, size: 24.0)
    }
    static var titleMedium: Font {
        return Font.custom(AppFonts.openSansExtraBold.rawValue, size: 18.0)
    }
    static var titleSmall: Font {
        return Font.custom(AppFonts.openSansBold.rawValue, size: 14.0)
    }

    /// Body Fonts
    static var bodyExtraLarge: Font {
        return Font.custom(AppFonts.openSansRegular.rawValue, size: 24.0)
    }
    static var bodyLarge: Font {
        return Font.custom(AppFonts.openSansRegular.rawValue, size: 18.0)
    }
    static var bodyLargeStrong: Font {
        return Font.custom(AppFonts.openSansBold.rawValue, size: 18.0)
    }
    static var bodyMedium: Font {
        return Font.custom(AppFonts.openSansRegular.rawValue, size: 14.0)
    }
    static var bodyMediumStrong: Font {
        return Font.custom(AppFonts.openSansBold.rawValue, size: 14.0)
    }
    static var bodySmall: Font {
        return Font.custom(AppFonts.openSansRegular.rawValue, size: 12.0)
    }
    static var bodySmallStrong: Font {
        return Font.custom(AppFonts.openSansBold.rawValue, size: 12.0)
    }
}
