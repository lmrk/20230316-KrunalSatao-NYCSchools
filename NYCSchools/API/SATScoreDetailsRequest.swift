//
//  SATScoreDetailsRequest.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 16/03/23.
//

import Foundation
struct SATScoreDetailsRequest: APIRequest {
    typealias Response = [SATScoreDetails]
    let dbn: String
    var path: String { Constants.APIPath.satScores+"\(dbn)" }
    var method: String { "GET" }
    let parameters: [String: Any] = [:]
    var headers: [String: String] {
        ["Content-Type": "application/json",
         "X-App-Token": Constants.Keys.apiKey]
    }
}
