//
//  SchoolListRequest.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 16/03/23.
//

import Foundation


struct SchoolListRequest: APIRequest {
    typealias Response = [School]
    
    var path: String { Constants.APIPath.schoolList }
    var method: String { "GET" }
    let parameters: [String: Any] = [:]
    var headers: [String: String] {
        ["Content-Type": "application/json",
         "X-App-Token":Constants.Keys.apiKey]        
    }
}


