//
//  WebServiceManager.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 15/03/23.
//

import Foundation


// Define an enumeration for network environment
enum NetworkEnvironment {
    case production
    case staging
    case development
    
    var baseURL: String {
        switch self {
        case .production:
            return "https://data.cityofnewyork.us"
        case .staging:
            return "https://data.cityofnewyork.us"
        case .development:
            return "https://data.cityofnewyork.us"
        }
    }
}

// Define a protocol for API requests
protocol APIRequest {
    associatedtype Response: Decodable
    
    var path: String { get }
    var method: String { get }
    var parameters: [String: Any] { get }
    var headers: [String: String] { get }
}


