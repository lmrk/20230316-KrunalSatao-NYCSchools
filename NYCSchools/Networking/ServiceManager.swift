//
//  NetworkManager.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 15/03/23.
//

import Foundation
import SystemConfiguration

protocol ServiceManagerProtocol {
    func send<T: APIRequest>(_ request: T, completion: @escaping (Result<T.Response, Error>) -> Void) 
}


class ServiceManager: ServiceManagerProtocol {
    private init() {}
    static let shared = ServiceManager()
    var environment: NetworkEnvironment {
#if DEBUG
        return .development
#else
        return .production
#endif
    }
    
    func send<T: APIRequest>(_ request: T, completion: @escaping (Result<T.Response, Error>) -> Void) {
        guard isNetworkReachable() else {
            DDLogger.sharedLogger.send(AppError.noInternet.localizedDescription, logLevel: .error)
            completion(.failure(AppError.noInternet))
            return
        }
        
        guard let url = URL(string: environment.baseURL + request.path) else {
            DDLogger.sharedLogger.send(AppError.invalidURL.localizedDescription, logLevel: .error)
            completion(.failure(AppError.invalidURL))
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = request.method
        urlRequest.allHTTPHeaderFields = request.headers
        
        if !request.parameters.isEmpty {
            urlRequest.httpBody = try? JSONSerialization.data(withJSONObject: request.parameters, options: [])
        }
    
        DDLogger.sharedLogger.send("API: \(url)", logLevel: .debug)
        
        let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            if let error = error {
                DDLogger.sharedLogger.send(error.localizedDescription, logLevel: .error)
                completion(.failure(error))
                
            } else {
                guard let data = data, let code = (response as? HTTPURLResponse)?.statusCode else {
                    DDLogger.sharedLogger.send(AppError.unexpectedResponse.localizedDescription, logLevel: .error)
                    completion(.failure(AppError.unexpectedResponse))
                    return
                }
                switch code {
                case -200 ..< 300:
                    do {
                        let decoder = JSONDecoder()
                        let result = try decoder.decode(T.Response.self, from: data)
                        DDLogger.sharedLogger.send("Response Received : \n \(result)", logLevel: .debug)
                        completion(.success(result))
                    } catch {
                        DDLogger.sharedLogger.send(error.localizedDescription, logLevel: .error)
                        completion(.failure(error))
                    }
                default:
                    completion(.failure(AppError.httpCode(code)))
                    break
                }
                
            }
        }
        
        task.resume()
    }
    
    // Define a function to check network reachability
    func isNetworkReachable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }), var flags = SCNetworkReachabilityFlags(rawValue: 0) as SCNetworkReachabilityFlags? else {
            return false
        }
        
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
}
