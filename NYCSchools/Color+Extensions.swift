//
//  RounderCorners.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 16/03/23.
//

import SwiftUI

extension Color {

    /// These are Colors defined in the Deloitte Dot template
    static let neutralBlack = Color(UIColor(named: "Neutral-Black")!)
    static let neutralClay = Color(UIColor(named: "Neutral-Clay")!)
    static let darkTextPrimary = Color(UIColor(named: "DarkText-Primary")!)
}

