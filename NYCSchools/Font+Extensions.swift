//
//  RounderCorners.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 16/03/23.
//

import SwiftUI

extension Font {

    /// Title Fonts
    static var titleLarge: Font {
        return Font.system(.body, weight:.heavy)
    }
    static var titleMedium: Font {
        return Font.system(.caption, weight: .medium)
    }
    static var titleSmall: Font {
        return Font.system(.title3, weight: .medium)
    }

}
