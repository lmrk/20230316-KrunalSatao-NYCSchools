//
//  School.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 15/03/23.
//

import Foundation

struct School: Codable, Identifiable, Hashable {
    var id: String { dbn }
    
    let dbn: String
    let schoolName: String
    let overviewParagraph: String
    let academicOpportunities1: String?
    let academicOpportunities2: String?
    let neighborhood: String?
    let location: String?
    let phoneNumber: String?
    let faxNumber: String?
    let schoolEmail: String?
    let website: String?
    let subway: String?
    let bus: String?
    let finalGrades: String?
    let totalStudents: String?
    let extracurricularActivities: String?
    let schoolSports: String?
    let attendanceRate: String?
    let requirement11: String?
    let requirement21: String?
    let requirement31: String?
    let requirement41: String?
    let requirement51: String?
    let offerRate1: String?
    let program1: String?
    let interest1: String?
    let method1: String?
    let admissionspriority11: String?
    let admissionspriority21: String?
    let admissionspriority31: String?
    let school_accessibility_description: String?
    let borough: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case academicOpportunities1 = "academicopportunities1"
        case academicOpportunities2 = "academicopportunities2"
        case neighborhood
        case location
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case schoolEmail = "school_email"
        case website
        case subway
        case bus
        case finalGrades = "finalgrades"
        case totalStudents = "total_students"
        case extracurricularActivities = "extracurricular_activities"
        case schoolSports = "school_sports"
        case attendanceRate = "attendance_rate"
        case requirement11 = "requirement11"
        case requirement21 = "requirement21"
        case requirement31 = "requirement31"
        case requirement41 = "requirement41"
        case requirement51 = "requirement51"
        case offerRate1 = "offerRate1"
        case program1 = "program1"
        case interest1 = "interest1"
        case method1 = "method1"
        case admissionspriority11 = "admissionspriority11"
        case admissionspriority21 = "admissionspriority21"
        case admissionspriority31 = "admissionspriority31"
        case school_accessibility_description = "school_accessibility_description"
        case borough = "borough"
    }
}
