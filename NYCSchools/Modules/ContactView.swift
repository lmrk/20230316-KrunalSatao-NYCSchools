//
//  ContactView.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 16/03/23.
//

import SwiftUI
import Foundation

/// Struct to displsy School Address and other Contact details
struct ContactView: View {

    // MARK: Variables =
    var viewModel: ContactViewModel

    // MARK: View Body -
    var body: some View {
        RoundedCardComponent {
            VStack (alignment: .leading, spacing: Constants.Spacing.leading) {
                SectionHeaderTextView(text: "Contact Details")
                if let location = viewModel.school.location, let address = location.components(separatedBy: "(").first {
                    HStack {
                        Text(Image(systemName: "location")).font(.system(size: Constants.Spacing.imageSize))
                        SecondaryTextView(text: address)
                    }
                }
                
                if let email = viewModel.school.schoolEmail {
                    HStack {
                        Text(Image(systemName: "envelope")).font(.system(size: Constants.Spacing.imageSize))
                        LinkTextView(text: email)
                    }
                }
                
                
                if let phoneNumber = viewModel.school.phoneNumber {
                    HStack {
                        Text(Image(systemName: "phone")).font(.system(size: Constants.Spacing.imageSize))
                        LinkTextView(text: phoneNumber)
                            .onTapGesture {
                                openURL(urlString: "tel://" + phoneNumber)
                            }
                    }
                }
                
                if let website = viewModel.school.website {
                    HStack {
                        Text(Image(systemName: "network")).font(.system(size: Constants.Spacing.imageSize))
                        LinkTextView(text: website)
                            .onTapGesture {
                                let urlString = "https://" + website
                                openURL(urlString: urlString)
                            }
                    }
                }
            }
        }
    }
}

extension ContactView {
    /// Enum to hold list of Localizable Strings required for School List Screen
    enum LocalizableStrings: String {
        case addressAndContact = "Address and Contact"

        var description: String {
            self.rawValue
        }
    }
    
    func openURL(urlString: String) {
        guard let url = URL(string: urlString) else {
            DDLogger.sharedLogger.send("Please check Phone Number", logLevel: .error)
            return
        }
        UIApplication.shared.open(url)
    }
}

