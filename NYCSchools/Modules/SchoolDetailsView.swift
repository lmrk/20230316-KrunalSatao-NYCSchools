//
//  SchoolInfoView.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 16/03/23.
//

import Foundation
import SwiftUI

struct SchoolDetailsView: View {
    
    // MARK: Variables -
    @ObservedObject var viewModel: SchoolDetailsViewModel
    
    // MARK: View Body -
    var body: some View {
        VStack {
            if viewModel.isLoading {
                ProgressView()
                    .padding(.top, Constants.Spacing.padding)
            } else if let error = viewModel.error {
                Text(error.localizedDescription)
                    .foregroundColor(.red)
                    .padding()
            } else {
                ScrollView {
                    ZStack {
                        VStack(alignment: .leading, spacing: Constants.Spacing.leading) {
                            PrimaryTextView(text: self.viewModel.school.schoolName)
                            RoundedCardComponent {
                                AnyView(
                                    VStack(alignment: .leading, spacing: Constants.Spacing.leading) {
                                        SectionHeaderTextView(text: "Overview")
                                        SecondaryTextView(text: self.viewModel.school.overviewParagraph)
                                    }
                                )
                            }
                            
                            if let satScores = self.viewModel.satScoreDetails {
                                RoundedCardComponent {
                                    AnyView(
                                        VStack(alignment: .leading, spacing: Constants.Spacing.leading) {
                                            SectionHeaderTextView(text: "Average SAT Scores")
                                            HStack {
                                                Text(Image(systemName: "x.squareroot")).font(.system(size: Constants.Spacing.imageSize))
                                                SecondaryTextView(text: satScores.satMathAvgScore)
                                                
                                                Text(Image(systemName: "book")).font(.system(size: Constants.Spacing.imageSize))
                                                SecondaryTextView(text: satScores.satCriticalReadingAvgScore)
                                                
                                                Text(Image(systemName: "pencil")).font(.system(size: Constants.Spacing.imageSize))
                                                SecondaryTextView(text: satScores.satWritingAvgScore)
                                                                                                
                                                Text(Image(systemName: "person.3")).font(.system(size: Constants.Spacing.imageSize))
                                                SecondaryTextView(text: satScores.numOfSatTestTakers)
                                            }
                                        }
                                    )
                                }
                            }
                            
                            ContactView(viewModel: ContactViewModel(school: self.viewModel.school))
                            
                        }
                        .padding(.leading, Constants.Spacing.padding)
                        .padding(.trailing, Constants.Spacing.padding)
                    }
                }
            }
        }
        
        .onAppear {
            viewModel.fetchSATScoreDetails()
        }
    }
}
