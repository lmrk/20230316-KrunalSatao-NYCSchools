//
//  SchoolDetailsViewModel.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 16/03/23.
//

import Foundation

class SchoolDetailsViewModel: ObservableObject {
    @Published var school: School
    @Published var satScoreDetails: SATScoreDetails? = nil
    
    @Published var isLoading = false
    @Published var error: Error? = nil
    // Make service manager mockable and testable
    private let serviceManager: ServiceManagerProtocol
    
    init(serviceManager: ServiceManagerProtocol = ServiceManager.shared, school: School) {
        self.serviceManager = serviceManager
        self.school = school
    }
    
    func fetchSATScoreDetails() {
        isLoading = true
        
        self.serviceManager.send(SATScoreDetailsRequest(dbn: school.dbn)) { [weak self] result in
            DispatchQueue.main.async {
                self?.isLoading = false
                
                switch result {
                case .success(let response):
                    guard let satDetails = response.first else {
                        self?.error = AppError.emptyData
                        DDLogger.sharedLogger.send("SAT Details not available for \(String(describing: self?.school.schoolName))", logLevel: .error)
                        return
                    }
                    self?.satScoreDetails = satDetails
                case .failure(let error):
                    self?.error = error
                }
            }
        }
    }
}
