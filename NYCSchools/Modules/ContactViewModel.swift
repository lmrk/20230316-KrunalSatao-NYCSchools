//
//  ContactViewModel.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 16/03/23.
//

import Foundation

class ContactViewModel: ObservableObject {
    var school: School
    
    init(school: School) {
        self.school = school
    }
}
