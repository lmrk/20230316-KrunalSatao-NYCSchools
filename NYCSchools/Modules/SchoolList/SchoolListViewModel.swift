//
//  SchoolListViewModel.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 16/03/23.
//

import Foundation
import Combine

class SchoolListViewModel: ObservableObject {
    @Published var schools: [School] = []
    @Published var isLoading = false
    @Published var error: Error?
    
    // Make service manager mockable and testable
    private let serviceManager: ServiceManagerProtocol
    
    init(serviceManager: ServiceManagerProtocol = ServiceManager.shared) {
        self.serviceManager = serviceManager
    }
    
    func fetchSchoolList() {
        isLoading = true
        
        self.serviceManager.send(SchoolListRequest()) { [weak self] result in
            DispatchQueue.main.async {
                self?.isLoading = false
                
                switch result {
                case .success(let response):
                    self?.schools = response
                case .failure(let error):
                    self?.error = error
                }
            }
        }
    }
}
