//
//  SchoolListView.swift
//  NYCSchools
//
//  Created by Satao, Krunal on 16/03/23.
//

import SwiftUI

struct SchoolListListView: View {
    @ObservedObject var viewModel = SchoolListViewModel()
    
    var body: some View {
        NavigationView {
            VStack {
                if viewModel.isLoading {
                    ProgressView()
                        .padding(.top, Constants.Spacing.padding)
                } else if let error = viewModel.error {
                    Text(error.localizedDescription)
                        .foregroundColor(.red)
                        .padding()
                } else {
                    List(viewModel.schools, id: \.dbn) { school in
                        ZStack {
                            RoundedCardComponent {
                                AnyView(
                                    VStack(alignment: .leading, spacing: Constants.Spacing.leading) {
                                        
                                        // School Name and neighbourhood
                                        PrimaryTextView(text: school.schoolName)
                                        SecondaryTextView(text: "Near " + (school.neighborhood ?? "") + ", \(school.borough ?? "")")
                                        
                                        // Emaiil
                                        HStack {
                                            Text(Image(systemName: "envelope")).font(.system(size: Constants.Spacing.imageSize))
                                            LinkTextView(text:school.schoolEmail ?? "")
                                        }
                                        
                                        // Grades, Total Students and accessibility
                                        HStack {
                                            Text(Image(systemName: "graduationcap.circle")).font(.system(size: Constants.Spacing.imageSize))
                                            if let grades = school.finalGrades, grades.components(separatedBy: "-") .count == 2 {
                                                SecondaryTextView(text: (school.finalGrades ?? ""))
                                            } else {
                                                SecondaryTextView(text: ("NA"))
                                            }
                                            
                                            Text(Image(systemName: "studentdesk")).font(.system(size: Constants.Spacing.imageSize))
                                            SecondaryTextView(text: (school.totalStudents ?? ""))
                                            
                                            Text(Image(systemName: "figure.roll")).font(.system(size: Constants.Spacing.imageSize))
                                            if let accessibilityLevel = school.school_accessibility_description, Int(accessibilityLevel) == 1 {
                                                SecondaryTextView(text: ("YES"))
                                            } else {
                                                SecondaryTextView(text: ("NO"))
                                            }
                                        }
                                    })
                            }
                            NavigationLink(destination: SchoolDetailsView(viewModel: SchoolDetailsViewModel(school: school))) {
                                EmptyView()
                            }
                            .opacity(.zero)
                        }
                        .listRowSeparator(.hidden)
                    }
                }
            }
            .navigationBarTitle("NYC Schools")
        }
        .onAppear {
            viewModel.fetchSchoolList()
        }
    }
}

