# **NY School List App**

This is a simple app that shows
* List of schools in NYC
* SAT Scores and additional details about school

# Structure
App is a combination of UIKit and SwiftUI, however, most of the heavy lifting is done by SwiftUI

App follows simple MVVM patten

```ServiceManager``` is used to make API calls from view models using ```ServiceManagerProtocol```

```APIRequest``` is a protocol to create request resource for each API


# Unit Tests

Unit tests are writtent to cover major business logic in view models.


Here a sample video

![Sample Video](AppScreens/demo.mp4)